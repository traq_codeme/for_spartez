import urllib3
import json


class bbapi_class():
    _http = urllib3.PoolManager()

    def __init__(self, limit, url):
        self._limit = int(limit)
        self._url = url

    def reops_list(self):
        r = self._http.request('GET', self._url)
        data = json.loads(r.data.decode('utf-8'))
        #print(json.dumps(data, sort_keys=True, indent=4))
        list_of_repos=[]
        for r in data['values']:
            list_of_repos.append(r['name'])
        list_of_repos = tuple(list_of_repos)
        return list_of_repos

    def get_commits(self):
        all_commits_bucket=[]
        for repo in self.reops_list():
            url = self._url+"/"+repo+"/commits"
            #print(url)
            r = self._http.request('GET', url)
            data = json.loads(r.data.decode('utf-8'))
            x = 0
            for r in data['values']:
                x += 1
                if x <= self._limit:
                    commit = r['author']['raw']
                    #print(json.dumps(r, sort_keys=True, indent=4))
                    #print(r['repository']['name'])
                    commit = str(commit)
                    commit = commit.split()
                    commit = commit[-1]
                    commit = commit.replace('<', '')
                    commit = commit.replace('>', '')
                    #print(commit)
                    all_commits_bucket.append(commit)
                else:
                    break
        return all_commits_bucket

    def commits_repo_counter(self, commiter):
        commiter = commiter
        counted_list = {}
        for repo in self.reops_list():
            repos_commits_bucket = []
            url = self._url+"/"+repo+"/commits"
            #print(url)
            r = self._http.request('GET', url)
            data = json.loads(r.data.decode('utf-8'))
            x = 0
            for r in data['values']:
                x += 1
                if x <= self._limit:
                    commit = r['author']['raw']
                    #print(json.dumps(r, sort_keys=True, indent=4))
                    #print(r['repository']['name'])
                    commit = str(commit)
                    commit = commit.split()
                    commit = commit[-1]
                    commit = commit.replace('<', '')
                    commit = commit.replace('>', '')
                    #print(commit)
                    repos_commits_bucket.append(commit)
                else:
                    break
            nr_of_commits = repos_commits_bucket.count(commiter)
            #print(repo, nr_of_commits)
            counted_list[repo] = nr_of_commits
        return counted_list


    def get_real_name(self, name):
        email = name

        for repo in self.reops_list():
            url = self._url+"/"+repo+"/commits"
            #print(url)
            r = self._http.request('GET', url)
            data = json.loads(r.data.decode('utf-8'))
            x = 0
            for r in data['values']:

                x += 1
                if x <= self._limit:
                    data = r['author']['raw']
                    author = data.split()
                    author = author[0]
                    mail = data.split()
                    mail = mail[-1]
                    mail = mail.replace('<', '')
                    mail = mail.replace('>', '')
                    if mail == email:
                        #print(author)
                        return author

                else:
                    break

#todo: I know this colud be better done, in one request to BB api, but i don't have time for grooming

#some tests
if __name__ == '__main__':
    abc = bbapi_class(10, 'https://api.bitbucket.org/2.0/repositories/tutorial')
    #print(abc.get_commits())
    #print(abc.commits_repo_counter('mary.example@rypress.com'))
    print(abc.get_real_name('mary.example@rypress.com'))